'use strict';
const MANIFEST = 'flutter-app-manifest';
const TEMP = 'flutter-temp-cache';
const CACHE_NAME = 'flutter-app-cache';
const RESOURCES = {
  "version.json": "1ebf78a12b4d481ce3898b779efc3d0f",
"index.html": "f354a95cc64446bbe9926a9753aa69bb",
"/": "f354a95cc64446bbe9926a9753aa69bb",
"main.dart.js": "f44782edb1f76748c5c5a7ee075ea542",
"favicon.png": "5dcef449791fa27946b3d35ad8803796",
"icons/Icon-192.png": "ac9a721a12bbc803b44f645561ecb1e1",
"icons/Icon-512.png": "96e752610906ba2a93c65f8abe1645f1",
"manifest.json": "99d7997d8ca9edc779563a30a0353204",
"assets/AssetManifest.json": "390a7866118665a492c8fb2f713e4bc6",
"assets/NOTICES": "27c7ee3475ac1286a4044aeac6bd57f2",
"assets/FontManifest.json": "dc3d03800ccca4601324923c0b1d6d57",
"assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "6d342eb68f170c97609e9da345464e5e",
"assets/packages/flutter_weather_bg/images/lightning4.webp": "a175b2bc15e4df4bab37d5ccc43c14a0",
"assets/packages/flutter_weather_bg/images/lightning2.webp": "f48a914f5b0560942aed70b7ea9efbf3",
"assets/packages/flutter_weather_bg/images/snow.webp": "d5ce493b018954f7eefb569fe185df05",
"assets/packages/flutter_weather_bg/images/lightning3.webp": "29797eeedec55c06efd7f167a74ec6ad",
"assets/packages/flutter_weather_bg/images/lightning0.webp": "8d28c99840abf552a78cade97a4cad22",
"assets/packages/flutter_weather_bg/images/sun.webp": "f4a3e24a77f84b97ac15f8c5b846eed1",
"assets/packages/flutter_weather_bg/images/lightning1.webp": "c267264ed06f875fa86b841e4e70064a",
"assets/packages/flutter_weather_bg/images/cloud.webp": "849a0a8f321c14b3af35ede9808a17b5",
"assets/packages/flutter_weather_bg/images/rain.webp": "be14922d4d3c0caa92982861045a678a",
"assets/fonts/MaterialIcons-Regular.otf": "1288c9e28052e028aba623321f7826ac",
"assets/assets/images/coldRisk.png": "013149821aa943d9e9b04203ee50f00f",
"assets/assets/images/comfort.png": "d426a90a24532de25eb3605252940115",
"assets/assets/images/ultraviolet.png": "7c9a6b51e8cd8a56da1407389df6c766",
"assets/assets/images/empty.png": "15e3f5e6a4c9022c9d856b08b6ef0e8c",
"assets/assets/images/carWashing.png": "de0326e0a48cfc137ae7b3b449c0a25a",
"assets/assets/images/caiyun.png": "40e69fb7ec01f5b0ecfbf343c9e646c0",
"assets/assets/images/typhoon.png": "84bf6d8be6dc0ee8c891eb49b17fe806",
"assets/assets/images/error.png": "cd7626899a81be2ead76fbc80195184b",
"assets/assets/images/ic_launcher.png": "b35b62dca9d7b91155dc6ee005975546",
"assets/assets/images/dressing.png": "8b3fb85925962666aa4f1c930b33ee24",
"assets/assets/images/weather/small_rain.png": "77bb0de0d74f3a68f03f043dd9da3939",
"assets/assets/images/weather/sunny_night.png": "c2b4a72936c4d373aa96bf50ac60ade0",
"assets/assets/images/weather/middle_snow.png": "022540e50c86c5399d24cbde48ad8618",
"assets/assets/images/weather/heavy_snow.png": "f9117d39a40d2f4b3f8219da3b49a705",
"assets/assets/images/weather/cloudy_night.png": "bd623d0d85d3f40f45b4ba466e6b8a5f",
"assets/assets/images/weather/sunrise.png": "3e6f07c029e06d04ee105a3520f9cdfd",
"assets/assets/images/weather/foggy.png": "4dd1065518fb3b0078ab183234f41b42",
"assets/assets/images/weather/small_snow.png": "14fd1571a568669db6c13828ad098bd8",
"assets/assets/images/weather/cloudy.png": "e0a9e40bffc7af6a9b2d1b16c112fd2f",
"assets/assets/images/weather/sunny.png": "54afbe2eedada3112bbe194c6dab7fd4",
"assets/assets/images/weather/middle_rain.png": "f1bf918ef5fe016c0ede7f616e620b95",
"assets/assets/images/weather/heavy_rain.png": "522ec7243426613f4069910d53901c62",
"assets/assets/images/weather/sandy.png": "0b87e307072059db621b330f77d87459",
"assets/assets/images/weather/overcast.png": "a116ad171dba692269edd27cc6987b77",
"assets/assets/images/weather/sunset.png": "744057c83b8a8a802d54829ef6ba2422",
"assets/assets/images/play.png": "de0d50bd24171253db5e0f4453ddbdfd"
};

// The application shell files that are downloaded before a service worker can
// start.
const CORE = [
  "/",
"main.dart.js",
"index.html",
"assets/NOTICES",
"assets/AssetManifest.json",
"assets/FontManifest.json"];
// During install, the TEMP cache is populated with the application shell files.
self.addEventListener("install", (event) => {
  self.skipWaiting();
  return event.waitUntil(
    caches.open(TEMP).then((cache) => {
      return cache.addAll(
        CORE.map((value) => new Request(value + '?revision=' + RESOURCES[value], {'cache': 'reload'})));
    })
  );
});

// During activate, the cache is populated with the temp files downloaded in
// install. If this service worker is upgrading from one with a saved
// MANIFEST, then use this to retain unchanged resource files.
self.addEventListener("activate", function(event) {
  return event.waitUntil(async function() {
    try {
      var contentCache = await caches.open(CACHE_NAME);
      var tempCache = await caches.open(TEMP);
      var manifestCache = await caches.open(MANIFEST);
      var manifest = await manifestCache.match('manifest');
      // When there is no prior manifest, clear the entire cache.
      if (!manifest) {
        await caches.delete(CACHE_NAME);
        contentCache = await caches.open(CACHE_NAME);
        for (var request of await tempCache.keys()) {
          var response = await tempCache.match(request);
          await contentCache.put(request, response);
        }
        await caches.delete(TEMP);
        // Save the manifest to make future upgrades efficient.
        await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
        return;
      }
      var oldManifest = await manifest.json();
      var origin = self.location.origin;
      for (var request of await contentCache.keys()) {
        var key = request.url.substring(origin.length + 1);
        if (key == "") {
          key = "/";
        }
        // If a resource from the old manifest is not in the new cache, or if
        // the MD5 sum has changed, delete it. Otherwise the resource is left
        // in the cache and can be reused by the new service worker.
        if (!RESOURCES[key] || RESOURCES[key] != oldManifest[key]) {
          await contentCache.delete(request);
        }
      }
      // Populate the cache with the app shell TEMP files, potentially overwriting
      // cache files preserved above.
      for (var request of await tempCache.keys()) {
        var response = await tempCache.match(request);
        await contentCache.put(request, response);
      }
      await caches.delete(TEMP);
      // Save the manifest to make future upgrades efficient.
      await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
      return;
    } catch (err) {
      // On an unhandled exception the state of the cache cannot be guaranteed.
      console.error('Failed to upgrade service worker: ' + err);
      await caches.delete(CACHE_NAME);
      await caches.delete(TEMP);
      await caches.delete(MANIFEST);
    }
  }());
});

// The fetch handler redirects requests for RESOURCE files to the service
// worker cache.
self.addEventListener("fetch", (event) => {
  if (event.request.method !== 'GET') {
    return;
  }
  var origin = self.location.origin;
  var key = event.request.url.substring(origin.length + 1);
  // Redirect URLs to the index.html
  if (key.indexOf('?v=') != -1) {
    key = key.split('?v=')[0];
  }
  if (event.request.url == origin || event.request.url.startsWith(origin + '/#') || key == '') {
    key = '/';
  }
  // If the URL is not the RESOURCE list then return to signal that the
  // browser should take over.
  if (!RESOURCES[key]) {
    return;
  }
  // If the URL is the index.html, perform an online-first request.
  if (key == '/') {
    return onlineFirst(event);
  }
  event.respondWith(caches.open(CACHE_NAME)
    .then((cache) =>  {
      return cache.match(event.request).then((response) => {
        // Either respond with the cached resource, or perform a fetch and
        // lazily populate the cache.
        return response || fetch(event.request).then((response) => {
          cache.put(event.request, response.clone());
          return response;
        });
      })
    })
  );
});

self.addEventListener('message', (event) => {
  // SkipWaiting can be used to immediately activate a waiting service worker.
  // This will also require a page refresh triggered by the main worker.
  if (event.data === 'skipWaiting') {
    self.skipWaiting();
    return;
  }
  if (event.data === 'downloadOffline') {
    downloadOffline();
    return;
  }
});

// Download offline will check the RESOURCES for all files not in the cache
// and populate them.
async function downloadOffline() {
  var resources = [];
  var contentCache = await caches.open(CACHE_NAME);
  var currentContent = {};
  for (var request of await contentCache.keys()) {
    var key = request.url.substring(origin.length + 1);
    if (key == "") {
      key = "/";
    }
    currentContent[key] = true;
  }
  for (var resourceKey of Object.keys(RESOURCES)) {
    if (!currentContent[resourceKey]) {
      resources.push(resourceKey);
    }
  }
  return contentCache.addAll(resources);
}

// Attempt to download the resource online before falling back to
// the offline cache.
function onlineFirst(event) {
  return event.respondWith(
    fetch(event.request).then((response) => {
      return caches.open(CACHE_NAME).then((cache) => {
        cache.put(event.request, response.clone());
        return response;
      });
    }).catch((error) => {
      return caches.open(CACHE_NAME).then((cache) => {
        return cache.match(event.request).then((response) => {
          if (response != null) {
            return response;
          }
          throw error;
        });
      });
    })
  );
}
